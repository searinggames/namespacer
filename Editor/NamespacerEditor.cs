﻿using Namespacer.Core;
using System.IO;
using UnityEditor;
using UnityEngine;


public class NamespacerEditor : EditorWindow
{
    private NamespacerSettings _namespacerSettings;

    private string _filename = "CustomBehaviour";

    private string _settingsPath = "Assets/Resources/NamespacerSettings.asset";

    private bool _isInterface = false;

    void Awake()
    {
        // Create the .asset object if it no longer exists (it's been deleted or moved).
        if (!File.Exists(_settingsPath))
        {
            Util.CreateSettingsAsset(_settingsPath);
        }
        
        _namespacerSettings = AssetDatabase.LoadAssetAtPath<NamespacerSettings>(_settingsPath);
    }

    void OnGUI()
    {
        GUILayout.Label("Namespacer settings:", EditorStyles.boldLabel);
        _namespacerSettings.rootFolder = EditorGUILayout.TextField("Root directory path: ", _namespacerSettings.rootFolder);
        _namespacerSettings.rootNamespace = EditorGUILayout.TextField("Root namespace: ", _namespacerSettings.rootNamespace); 

        GUILayout.Label("Create namespaced script:", EditorStyles.boldLabel);
        _filename = EditorGUILayout.TextField("Filename: ", _filename);

        _isInterface = GUILayout.Toggle(_isInterface, "Make interface");

        GUILayout.Label("");

        if (GUILayout.Button("Create Script"))
        {            
            NamespacerCore namespacerCore = new NamespacerCore(_namespacerSettings);

            if (namespacerCore.CreateScript(_filename, _isInterface))
            {
                this.Close();
            };            
        }

        _namespacerSettings.useUnityFormatting = GUILayout.Toggle(_namespacerSettings.useUnityFormatting, "Use Unity formatting :(");
    }

    [MenuItem("Assets/Create/Namespaced C# Script")]
    public static void CreateNamespaceScript()
    {
        EditorWindow.GetWindow(typeof(NamespacerEditor));        
    }
}
