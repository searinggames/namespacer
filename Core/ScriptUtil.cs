﻿using System.IO;
using UnityEditor;

namespace Namespacer.Core
{
    public partial class Util
    {
        public static void BuildScript(string filename, string path, string namespaceStr, bool isInterface, NamespacerSettings namespacerSettings)
        {            
            string destPath = path + "/" + filename + ".cs";

            if (isInterface)
            {
                BuildInterface(filename, namespaceStr, destPath, namespacerSettings);
            }
            else
            {
                if (namespacerSettings.useUnityFormatting)
                {
                    BuildUnityFormatScript(filename, namespaceStr, destPath);
                }
                else
                {
                    BuildStandardFormatScript(filename, namespaceStr, destPath);
                }
            }
           
            AssetDatabase.Refresh();
        }

        private string GetDestinationPath(string filename, string path)
        {
            string destPath = path + "/" + filename + ".cs";

            return destPath;
        }

        private static void BuildStandardFormatScript(string filename, string namespaceStr, string destPath)
        {
            using (StreamWriter outfile =
                new StreamWriter(destPath))
            {
                outfile.WriteLine("using UnityEngine;");
                outfile.WriteLine("using System.Collections;");
                outfile.WriteLine("");
                outfile.WriteLine("namespace " + namespaceStr);
                outfile.WriteLine("{");
                outfile.WriteLine("    public class " + filename + " : MonoBehaviour");
                outfile.WriteLine("    {");
                outfile.WriteLine("        // Use this for initialization");
                outfile.WriteLine("        void Start()");
                outfile.WriteLine("        {");
                outfile.WriteLine("");
                outfile.WriteLine("        }");
                outfile.WriteLine("");
                outfile.WriteLine("        // Update is called once per frame");
                outfile.WriteLine("        void Update()");
                outfile.WriteLine("        {");
                outfile.WriteLine("");
                outfile.WriteLine("        }");
                outfile.WriteLine("    }");
                outfile.WriteLine("}");
            }
        }

        private static void BuildUnityFormatScript(string filename, string namespaceStr, string destPath)
        {
            using (StreamWriter outfile =
                new StreamWriter(destPath))
            {
                outfile.WriteLine("using UnityEngine;");
                outfile.WriteLine("using System.Collections;");
                outfile.WriteLine("");
                outfile.WriteLine("namespace " + namespaceStr + " {");
                outfile.WriteLine("");
                outfile.WriteLine("    public class " + filename + " : MonoBehaviour {");
                outfile.WriteLine("");
                outfile.WriteLine("        // Use this for initialization");
                outfile.WriteLine("        void Start () {");
                outfile.WriteLine("");
                outfile.WriteLine("        }");
                outfile.WriteLine("");
                outfile.WriteLine("        // Update is called once per frame");
                outfile.WriteLine("        void Update () {");
                outfile.WriteLine("");
                outfile.WriteLine("        }");
                outfile.WriteLine("    }");
                outfile.WriteLine("}");
            }
        }
    }
}
