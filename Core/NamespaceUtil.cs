﻿using System.Text.RegularExpressions;
using UnityEditor;

namespace Namespacer.Core
{
    public partial class Util
    {
        public static bool IsValidNamespace(NamespacerSettings namespacerSettings)
        {
            //string pattern = @"[/?<>\:*|,.+-`'{}&%^#!=@;~]";
            string pattern = @"['{}[\]\\;':"",/?!@#$%&*()+=-]";

            return !Regex.IsMatch(namespacerSettings.rootNamespace, pattern);
        }

        public static string GetNamespace(string path, NamespacerSettings namespacerSettings)
        {
            string namespacePath = path;
            string rootNamespace = namespacerSettings.rootNamespace;

            if (rootNamespace == string.Empty)
            {
                rootNamespace = PlayerSettings.productName;
            }

            // Strip any whitespace out of the root namespace.
            rootNamespace = rootNamespace.Replace(" ", "");

            // Get the root directory.
            // Check if Assets/ is at the start of the path, and remove it.
            // It should always be but the check is here just in case.
            if (namespacePath.IndexOf("Assets") == 0)
            {
                namespacePath = path.Remove(0, 6);
            }

            // If we have a leading /, remove it.
            if (namespacePath.IndexOf("/") == 0)
            {
                namespacePath = namespacePath.Remove(0, 1);
            }

            var pathFolders = namespacePath.Split('/');
            var rootFolders = namespacerSettings.rootFolder.Split('/');

            // Check if the root folder exists in an exact match.
            // The root directory path assumes it comes immediately after Assets/, it doesn't search
            // for the first occurrence of it.     
            if (namespacePath.IndexOf(namespacerSettings.rootFolder) == 0)
            {
                int replaceLength = 0;

                for (int i = 0; i < rootFolders.Length; i++)
                {
                    // Make sure there's an item in pathFolders.
                    if ((pathFolders.Length - 1) >= i)
                    {
                        if (rootFolders[i] == pathFolders[i])
                        {
                            // Add the index counter to the length to account for the '/' we split on.
                            replaceLength += rootFolders[i].Length + i;
                        }
                        else
                        {
                            replaceLength = 0;
                            break;
                        }
                    }
                }

                namespacePath = namespacePath.Remove(0, replaceLength);
            }

            // If we have a leading /, remove it.
            if (namespacePath.IndexOf("/") == 0)
            {
                namespacePath = namespacePath.Remove(0, 1);
            }

            if (namespacePath.Length > 0)
            {
                namespacePath = namespacePath.Replace("/", ".");
                namespacePath = rootNamespace + "." + namespacePath;
            }
            else
            {
                namespacePath = rootNamespace;
            }

            return namespacePath;
        }
    }
}
