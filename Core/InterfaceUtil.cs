﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Namespacer.Core
{
    public partial class Util
    {
        public static void BuildInterface(string filename, string namespaceStr, string destPath, NamespacerSettings namespacerSettings)
        {
            if (namespacerSettings.useUnityFormatting)
            {
                BuildUnityFormatInterface(filename, namespaceStr, destPath);
            }
            else
            {
                BuildStandardFormatInterface(filename, namespaceStr, destPath);
            }
        }

        private static void BuildStandardFormatInterface(string filename, string namespaceStr, string destPath)
        {
            using (StreamWriter outfile =
                new StreamWriter(destPath))
            {
                outfile.WriteLine("using UnityEngine;");
                outfile.WriteLine("using System.Collections;");
                outfile.WriteLine("");
                outfile.WriteLine("namespace " + namespaceStr);
                outfile.WriteLine("{");
                outfile.WriteLine("    public interface " + filename);
                outfile.WriteLine("    {");
                outfile.WriteLine("");
                outfile.WriteLine("    }");
                outfile.WriteLine("}");
            }
        }

        private static void BuildUnityFormatInterface(string filename, string namespaceStr, string destPath)
        {
            using (StreamWriter outfile =
                new StreamWriter(destPath))
            {
                outfile.WriteLine("using UnityEngine;");
                outfile.WriteLine("using System.Collections;");
                outfile.WriteLine("");
                outfile.WriteLine("namespace " + namespaceStr + " {");
                outfile.WriteLine("");
                outfile.WriteLine("    public interface " + filename + " {");
                outfile.WriteLine("");      
                outfile.WriteLine("    }");
                outfile.WriteLine("}");
            }
        }
    }
}
