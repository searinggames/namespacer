﻿using UnityEditor;
using UnityEngine;

namespace Namespacer.Core
{
    [System.Serializable]
    public class NamespacerSettings : ScriptableObject
    {
        public string rootNamespace;

        public string rootFolder;

        public bool useUnityFormatting; 

        public void Init()
        {
            rootNamespace = PlayerSettings.productName;
            rootFolder = "";
            useUnityFormatting = false;            
        }
    }
}
